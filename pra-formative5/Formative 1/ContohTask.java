
class ContohTask implements Runnable {
    private int num;

    public ContohTask(int num) {
        this.num = num;
    }

    @Override
    public void run() {
        System.out.printf("Thread %d is running\n", this.num);
    }
}
