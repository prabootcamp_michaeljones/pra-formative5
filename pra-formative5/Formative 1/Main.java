import java.util.concurrent.*;

public class Main {
    public static final int NumberInt = Runtime.getRuntime().availableProcessors();

    public static void main(String[] args) {
        ExecutorService executor = Executors.newFixedThreadPool(NumberInt);

        for (int i = 14; i < 22; i++) {
            ContohTask task = new ContohTask(i);
            executor.submit(task);
        }

        executor.shutdown();
    }
}
