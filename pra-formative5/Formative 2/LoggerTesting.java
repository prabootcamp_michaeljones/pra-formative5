import java.util.logging.*;
import java.util.*;

public class LoggerTesting {
    public static Logger logger = Logger.getLogger(LoggerTesting.class.getName());

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String a, b;
        System.out.print(("Masukan Angka Pertama: "));
        a = input.nextLine();
        System.out.print(("Masukan Angka Kedua: "));
        b = input.nextLine();

        try {
            int x = Integer.parseInt(a);
            int y = Integer.parseInt(b);

            System.out.printf("Hasil : %d\n", x / y);
        } catch (NumberFormatException e) {
            System.out.println("Error: " + e.toString());
            logger.log(Level.SEVERE, "Hello SEVERE LoggerTesting, " + logger.getName());
        } catch (ArithmeticException e) {
            System.out.println("Error: " + e.toString());
            logger.log(Level.SEVERE, "Hello SEVERE LoggerTesting, " + logger.getName());
        } finally {
            logger.log(Level.INFO, "Hello INFO LoggerTesting, " + logger.getName());
        }
    }
}
